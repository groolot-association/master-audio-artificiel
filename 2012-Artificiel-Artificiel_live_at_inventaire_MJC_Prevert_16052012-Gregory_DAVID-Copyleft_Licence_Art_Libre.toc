CD_DA
CD_TEXT {
  LANGUAGE_MAP {
    0 : EN
  }
  LANGUAGE 0 {
    TITLE "Artificiel"
    PERFORMER "Gregory DAVID"
  }
}

TRACK AUDIO
COPY
NO PRE_EMPHASIS
CD_TEXT {
  LANGUAGE 0 {
     TITLE "Introduction"
     PERFORMER "Gregory DAVID"
  }
}
FILE "2012-Artificiel-Artificiel_live_at_inventaire_MJC_Prevert_16052012-Gregory_DAVID-Copyleft_Licence_Art_Libre.wav"  00:00:00 01:04:00
START 00:00:00

TRACK AUDIO
COPY
NO PRE_EMPHASIS
CD_TEXT {
  LANGUAGE 0 {
     TITLE "Performance"
     PERFORMER "Gregory DAVID"
  }
}
FILE "2012-Artificiel-Artificiel_live_at_inventaire_MJC_Prevert_16052012-Gregory_DAVID-Copyleft_Licence_Art_Libre.wav"  01:04:00 37:06:00
START 00:00:00
