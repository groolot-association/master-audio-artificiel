CD_DA
CD_TEXT {
  LANGUAGE_MAP {
    0 : EN
  }
  LANGUAGE 0 {
    TITLE "Artificiel"
    PERFORMER "Gregory DAVID"
  }
}

TRACK AUDIO
COPY
NO PRE_EMPHASIS
CD_TEXT {
  LANGUAGE 0 {
     TITLE "Live \340 EVE"
     PERFORMER ""
  }
}
FILE "2015-Artificiel-Artificiel_live_EVE_20140228_et_emmetrop_nadir_20131031-Gregory_DAVID-Copyleft_Licence_Art_Libre.wav"  00:00:00 37:20:00
START 00:00:00

TRACK AUDIO
COPY
NO PRE_EMPHASIS
CD_TEXT {
  LANGUAGE 0 {
     TITLE "interlude"
     PERFORMER ""
  }
}
FILE "2015-Artificiel-Artificiel_live_EVE_20140228_et_emmetrop_nadir_20131031-Gregory_DAVID-Copyleft_Licence_Art_Libre.wav"  37:20:00 02:53:00
START 00:00:00

TRACK AUDIO
COPY
NO PRE_EMPHASIS
CD_TEXT {
  LANGUAGE 0 {
     TITLE "Live au Nadir"
     PERFORMER ""
  }
}
FILE "2015-Artificiel-Artificiel_live_EVE_20140228_et_emmetrop_nadir_20131031-Gregory_DAVID-Copyleft_Licence_Art_Libre.wav"  40:13:00 35:17:00
START 00:00:00
